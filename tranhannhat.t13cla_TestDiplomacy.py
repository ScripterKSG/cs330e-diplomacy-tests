from io import StringIO

from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_solve, diplomacy_eval

class TestDiplomacy(TestCase):

    def test_read_1(self):
        s = ['A Madrid Hold\n']
        input_s = diplomacy_read(s)
        self.assertEqual(input_s, [['A', 'Madrid', 'Hold']])

    def test_eval_1(self):
        output = diplomacy_eval([['A', 'Madrid', 'Hold']])
        self.assertEqual(output, [['A', 'Madrid']])

    def test_eval_2(self):
        output = diplomacy_eval([['C', 'Saigon', 'support', 'B'], ['D', 'Houston', 'Support', 'A'],
                                 ['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'move', 'Madrid'] ])
        self.assertEqual(output, [['A', '[dead]'], ['B', '[dead]'], ['C', 'Saigon'], ['D', 'Houston']])

    def test_eval_3(self):
        output = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid']])
        self.assertEqual(output, [['A', '[dead]'], ['B', '[dead]']])

    def test_eval_4(self):
        output = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'],
                                 ['C', 'London', 'Hold']])
        self.assertEqual(output, [['A', '[dead]'], ['B', '[dead]'], ['C', 'London']])

    def test_eval_5(self):
        output = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'],
                                 ['C', 'Saigon', 'Move', 'Barcelona']])
        self.assertEqual(output, [['A', '[dead]'], ['B', '[dead]'], ['C', 'Barcelona']])

    def test_eval_6(self):
        output = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'],
                                 ['C', 'Saigon', 'Support', 'B'], ['D', 'Houston', 'Support', 'A']])
        self.assertEqual(output, [['A', '[dead]'], ['B', '[dead]'], ['C', 'Saigon'], ['D', 'Houston']])

    def test_eval_7(self):
        output = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'],
                                 ['C', 'London', 'Support', 'B'], ['D', 'Austin', 'Move', 'London']])
        self.assertEqual(output, [['A', '[dead]'], ['B', '[dead]'], ['C', '[dead]'], ['D', '[dead]']])

    def test_eval_8(self):
        output = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'],
                                 ['C', 'London', 'Support', 'B'], ['D', 'London', 'Support', 'A']])
        self.assertEqual(output, [['A', '[dead]'], ['B', '[dead]'], ['C', '[dead]'], ['D', '[dead]']])

    def test_eval_9(self):
        output = diplomacy_eval([['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'],
                                 ['C', 'Austin', 'Move', 'Madrid']])
        self.assertEqual(output, [['A', '[dead]'], ['B', '[dead]'], ['C', '[dead]']])

    def test_eval_10(self):
        output = diplomacy_eval([['A', 'Madrid', 'Hold'],['B', 'Barcelona', 'Move', 'Madrid'],['C', 'London', 'Move', 'Madrid'],
     ['D', 'Paris', 'Support', 'B'],['E', 'Dublin', 'Support', 'D'],['F', 'Dallas', 'Move', 'Paris']])
        self.assertEqual(output, [['A', '[dead]'], ['B', '[dead]'], ['C', '[dead]'], ['D', 'Paris'], ['E', 'Dublin'], ['F', '[dead]']])

    def test_print1(self):
        w = StringIO()
        diplomacy_print(w, [['A', '[dead]']])
        self.assertEqual(w.getvalue(), "A [dead]\n")

    def test_print2(self):
        w = StringIO()
        diplomacy_print(w, [['A', '[dead]'],['B', '[dead]']])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_print3(self):
        w = StringIO()
        diplomacy_print(w, [['A', '[dead]'],['B', '[dead]'], ['C', 'Paris']])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Paris\n")

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

if __name__ == "__main__":
    main()